<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Route::get('/routing', 'Controller@function');
| 
*/

///Route::get('/page-1', 'IndexController@index');
Route::get('/page-1', function () {
        return view('CheckIn');
    }
);

Route::get('/page-2', function () {
    return view('BookingCode');
    }
);

Route::get('/page-3', function () {
    return view('FAQ');
    }
);

Route::get('/page-4', function () {
    return view('EnjoyCheckIn');
    }
);

Route::get('/page-5', function () {
    return view('Slider');
    }
);

Route::get('/page-6', function () {
    return view('BookingRef');
    }
);
Route::get('/page-7', function () {
    return view('HelpVC');
    }
);

Route::get('/page-8', function () {
    return view('StartCheckin');
    }
);
Route::get('/page-9', function () {
    return view('welcome');
    }
);
Route::get('/page-10', function () {
    return view('FormCheckout');
    }
);

Route::get('/page-11', function () {
    return view('CheckInPhoto');
    }
);

Route::get('/page-12', function () {
    return view('ContactUs');
    }
);
Route::get('/page-13', function () {
    return view('CheckInScroll');
    }
);

Route::get('/page-14', function () {
    return view('FAQ2');
    }
);

Route::get('/page-15', function () {
    return view('AboutUs');
    }
);

Route::get('/page-16', function () {
    return view('Checkout');
    }
);
Route::get('/page-17', function () {
    return view('FAQ1');
    }
);