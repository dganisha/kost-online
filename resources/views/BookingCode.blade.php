@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right">
      <p class="text-white letter fs-12">BOOKING CODE ERROR</p>
  </div>
  <div class="col-9">
      <div>
          <p class="text-white letter-4 fs-14 font-weight-light">Sorry, We cannot find your Booking Code. Please check your code again, and/or please be reminded that your check in time is at 2pm.</p>
          <button class="btn-trans-default text-white letter-4 px-3 py-2">
            TRY AGAIN
            </button>
      </div>
  </div>
@endsection