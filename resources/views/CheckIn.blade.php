@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right">
      <p class="text-white letter fs-12 font-weight-light">CHECK IN</p>
  </div>
  <div class="col-9">
      <div>
          <p class="text-white letter-4 fs-14 font-weight-light">I, Hereby Agree to all the terms and conditions and have provided the correct informations</p>
          <textarea class="w-100 mb-2 b-grey border-0 p-3" name="" id="" rows="8">

          </textarea>
          <p class="text-white letter-4 fs-14 font-weight-light">Please sign inside the box.</p>
      </div>
  </div>
  <div class="col-12 my-5">
      <div class="row">
          <div class="col-6">
              <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
                  BACK
              </button>
          </div>
          <div class="col-6 text-right">
              <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
              NEXT
              </button>
          </div>
      </div>
  </div>
@endsection