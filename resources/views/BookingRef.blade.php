@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right">
      <p class="text-white letter fs-12">CHECK IN</p>
  </div>
  <div class="col-9">
      <div>
          <p class="text-white letter-4 fs-14 font-weight-light">Please key in your BOOKING REFERENCE NUMBER.</p>
          <input type="text" class="input-default px-3 py-2 fs-14 check_in">
      </div>
  </div>
@endsection