<html>
    <head>
        <title>Layout master</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="{{ asset('assets') }}/global.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    </head>
    <body>
        <div class="row mx-0">
            <div class="col-4 vh-100 bg-dark-grey text-white py-4 px-4">
                <div class="row">
                    <div class="col-12">
                        <p class="float-left letter fs-12 font-weight-light">HOME</p>
                        <p class="float-right letter fs-12 font-weight-light">ABOUT US</p>
                    </div>
                </div>
                <div class="row" style="height: 80%">
                    <div class="col-6 my-auto">
                    <img src="{{asset('assets')}}/screen_shoot.png" alt="">
                    </div>
                    <div class="col-6 my-auto position-relative">
                        <p class="position-absolute social_media fs-12 font-weight-light" style="height:196px;">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                            <i class="fa fa-info" aria-hidden="true"></i>
                       SOSIAL MEDIA</p>
                    </div>
                </div>
                <div class="row" style="height: 18%;">
                    <div class="col-12 my-auto">
                        <p class="float-left mb-0 letter fs-12 font-weight-light">INFORMATION</p>
                        <p class="float-right mb-0 letter fs-12 font-weight-light">CONTACT US</p>
                    </div>
                </div>
            </div>
            <div class="col-8 vh-100 positon-relative">
                <p class="fs-12 letter text-white position-absolute text_hanging">MON MAR 16 2020 8:47 PM <i class="fa fa-search" aria-hidden="true"></i></p>
                <div class="row">
                    <div class="slider">
                        <div class="col-4 px-0 vh-100">
                            <img class="h-100 w-100" src="{{ asset('assets') }}/img/cS-1.jpg" alt="">
                        </div>
                        <div class="col-4 px-0 vh-100">
                            <img class="h-100 w-100" src="{{ asset('assets') }}/img/cS-2.jpg" alt="">
                        </div>
                        <div class="col-4 px-0 vh-100">
                            <img class="h-100 w-100" src="{{ asset('assets') }}/img/cS-3.jpg" alt="">
                        </div>
                    </div>
                </div>
                <p class="float-right letter fs-12 text-white position-absolute text_hanging_bottom">
                    <span class="mr-2">FAQ</span>
                    <span>HELP</span>
                </p>
            </div>
        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.slider').bxSlider({
                auto: true,
                pager:false,
                pause: 2000
            });
        });
      </script>
</html>