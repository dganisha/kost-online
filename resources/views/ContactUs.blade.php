@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right mb-4">
      <p class="text-white letter fs-12">CONTACT US</p>
  </div>
  <div class="col-12">
        <ul class="pl-0 list-none text-white letter-4 fs-14 font-weight-light">
            <li>COSIN | SMART KOST</li>
            <li>Cendana Vill no. 48</li>
            <li>Jatayu Garut</li>
            <li>West Java</li>
        </ul>
        <div class="col-12 text-right px-0">
            <p class="text-white letter-4 fs-14 font-weight-light">BRANCHES :</p>
        </div>
        <div class="row mb-3">
            <div class="col-6">
                <ul class="pl-0 list-none text-white letter-4 fs-14 font-weight-light">
                    <li>Branch 1</li>
                    <li>Jend. Ahmad Yani 19</li>
                    <li>Padaringan</li>
                    <li>Cianjur</li>
                    <li>West Java 61125</li>
                </ul>
            </div>
            <div class="col-6 text-right">
                <ul class="pl-0 list-none text-white letter-4 fs-14 font-weight-light">
                    <li>Branch 2</li>
                    <li>Mekarwangi 38</li>
                    <li>Komplek Istana Bunga</li>
                    <li>Kiyai Maja</li>
                    <li>Majalengka</li>
                    <li>West Java</li>
                </ul>
            </div>
        </div>
        <div class="col-12 px-0">
            <p class="text-white letter-4 fs-14 font-weight-light">MAIN OFFICE :</p>
            <ul class="pl-0 list-none text-white letter-4 fs-14 font-weight-light">
                <li>COSIN | SMART KOST</li>
                <li>Bukit Siliwangi no. 20</li>
                <li>Jayakarta Garut</li>
                <li>West Java</li>
                <li>08123456789</li>
            </ul>
        </div>

  </div>
@endsection