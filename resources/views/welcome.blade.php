@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right">
      <p class="text-white letter fs-12"></p>
  </div>
  <div class="col-12 text-right">
      <div class="text-white letter-3 fs-12">
          <p class="text-white letter-4 fs-14 font-weight-light my-5">WELCOME TO</p>
          <p class="text-white fs-30" style="letter-spacing: 65px; margin-right:-64px;">COSIN</p>
          <p class="text-white fs-24" style="letter-spacing: 26px; margin-right:-25px;">smart kost</p>
      </div>
      <div class="row">
        <div class="col-6">
            
        </div>
        <div class="col-6 text-right">
            <div class="row">
                <div class="col-6 text-left pl-0">
                    <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
                        CHECK IN
                        </button>
                </div>
                <div class="col-6">

                <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
                    CHECK OUT
                    </button>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection