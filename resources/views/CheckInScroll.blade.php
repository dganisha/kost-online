@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right mb-4">
      <p class="text-white letter fs-12">CHECK IN</p>
  </div>
    <div class="col-12">
        <div class="wrapper_tc">
            <div class="row h-100">
                <div class="m-auto">
                    <p class="text-white letter-4 fs-12">T & C Scroll</p>
                </div>
            </div>
        </div>
    </div>
  <div class="col-12 my-5">
    <div class="row">
        <div class="col-6">
            <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
                BACK
            </button>
        </div>
        <div class="col-6 text-right">
            <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
            AGREE
            </button>
        </div>
    </div>
  </div>
@endsection