@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right">
      <p class="text-white letter fs-12 font-weight-light">CHECK IN</p>
  </div>
  <div class="col-10">
      <div>
          <p class="text-white letter-4 fs-14 font-weight-light">Please align your face accordingly and together with your ID Card</p>
          <div class="w-50 mb-2 b-grey border-0 p-3 wrapper_photo">
              <div class="row h-100">
                  <div class="m-auto text-center">
                    <i class="fa fa-camera fa-2x text-white"></i>
                    <p class="text-white letter-4 fs-14 font-weight-light my-2">Take Picture</p>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="col-12 my-5">
      <div class="row">
          <div class="col-6">
              <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
                  BACK
              </button>
          </div>
          <div class="col-6 text-right">
              <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
              NEXT
              </button>
          </div>
      </div>
  </div>
@endsection