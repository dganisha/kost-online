@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right mb-4">
      <p class="text-white letter fs-12">HELP</p>
  </div>
  <div class="col-12">
      <div>
          <p class="text-white letter-4 fs-14 font-weight-light">You can first check the FAQ section or the information Section. But if you still cannot find your answer and need some human help. we are going to assist your using Video Call with our Cosin Crew.</p>
          <button class="btn-trans-default text-white letter-4 px-3 py-2">
            START VIDEO CALL
          </button>
      </div>
  </div>
@endsection