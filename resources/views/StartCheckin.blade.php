@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right">
      <p class="text-white letter fs-12">CHECK IN</p>
  </div>
  <div class="col-9">
      <div class="text-white letter-3 fs-12">
          <p class="text-white letter-4 fs-14 font-weight-light">Welcome to COSIN SMART Kost</p>
          <p class="fs-20 text-white letter-4 font-weight-light">Mr./Mrs./Ms.</p>
          <ul class="pl-0 list-none text-white letter-4 fs-14 font-weight-light">
            <li>Check In Date :</li>
            <li>Check Out Date :</li>
          </ul>
          <p class="text-white letter-4 fs-14 font-weight-light my-5">Please Input Your</p>

          <ul class="pl-0 list-none text-white letter-4 fs-14 font-weight-light">
            <li>Email :</li>
            <li>Phone :</li>
            <li>Instagram :</li>
          </ul>
      </div>
  </div>
  <div class="col-12 my-5">
    <div class="row">
        <div class="col-6">
            <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
                BACK
            </button>
        </div>
        <div class="col-6 text-right">
            <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
            NEXT
            </button>
        </div>
    </div>
</div>
@endsection