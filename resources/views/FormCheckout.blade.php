@extends('master.app')
@section('content')
   <div class="col-12 mt-5 text-right mb-4">
      <p class="text-white letter fs-12">CHECK OUT</p>
  </div>
  <div class="col-12">
      <p class="text-white letter-4 fs-14 font-weight-light mb-5">Please key in your</p>
      <div class="row">
          <div class="col-8">
              <div class="row my-3">
                    <div class="col-4 my-auto">
                        <span class="text-white letter-4 fs-14 font-weight-light mb-5">ROOM NUMBER </span>
                    </div>
                    <div class="col-6">
                    <span class="text-white mr-2">:</span><input type="text" class="input-default px-3 py-2 fs-14 check_in"> 
                    </div>
              </div>
              <div class="row my-3">
                    <div class="col-4 my-auto">
                        <span class="text-white letter-4 fs-14 font-weight-light mb-5">PASSWORD</span>
                    </div>
                    <div class="col-6">
                        <span class="text-white mr-2">:</span><input type="password" class="input-default px-3 py-2 fs-14 check_in"> 
                    </div>
              </div>
              <p class="text-white letter-4 fs-14 font-weight-light mt-4">Note : please be remindee once the room is checked out. the password cannot be used anymore.</p>

              <p class="text-white letter-4 fs-14 font-weight-light">Kindly recheck your personal belongings.</p>
          </div>
      </div>
  </div>
  <div class="col-12 my-5">
    <div class="row">
        <div class="col-6">
            <button class="btn-trans-default text-white letter-4 px-3 py-2 font-weight-light">
            CHECK OUT
            </button>
        </div>
        <div class="col-6"></div>
    </div>
  </div>
@endsection