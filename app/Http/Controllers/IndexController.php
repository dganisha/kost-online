<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    function index() {
        // $condition = true;
        // $result = $condition ? "say hai" : "say no";
        // return view('master.app', ["result" => $result]);
        return view('welcome');
    }

    function home() {
        return view('home'); 
    }

    function profile() {
        return view('profile'); 
    }

    function pinjam() {
        $users = DB::table('users')->first();
        return view('welcome', ["users" => $users]);
    }
}
